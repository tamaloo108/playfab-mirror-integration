﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class LoginPanelUI : MonoBehaviour
{

    public ClientStartUp clientStartUp;
    public ServerStartUp serverStartUp;

    public Button loginButton;
    public Button startLocalServerButton;


    [SerializeField] NetworkManager _manager;

    public void StartLogin()
    {
        _manager.StartClient();
    }

}
