using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using PlayFab.MultiplayerAgent.Model;
using PlayFab;
using UnityEngine.Events;
using PlayFab.MultiplayerModels;
using System;
using STVR.CDR.ServerList;

public class TankNetworkManager : NetworkManager
{
    [SerializeField] BuildMode ClientMode;

    /// <summary>
    /// Specified remote server build id, required to get port when on remote client mode.
    /// </summary>
    [SerializeField] string ServerBuildID = "";

    [SerializeField] TelepathyTransport telepathy;
    [SerializeField] kcp2k.KcpTransport kcp;

    #region Client Connection Setup
#if ENABLE_PLAYFABCLIENT_API
    public static TankNetworkManager Instance { get; private set; }

    public ConnectedEvent OnConnected = new ConnectedEvent();
    public DisconnectedEvent OnDisconnected = new DisconnectedEvent();

    public class ConnectedEvent : UnityEvent { }
    public class DisconnectedEvent : UnityEvent<int?> { }

    public override void Awake()
    {
        base.Awake();
        Instance = this;
    }

    public override void Start()
    {
        base.Start();
        this.StartClient();
    }


    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
        Debug.Log("connected");
        OnConnected.Invoke();
    }


    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        Debug.Log("disconnected");
        OnDisconnected.Invoke(null);
    }

    /// <summary>
    /// Called on clients when a network error occurs.
    /// </summary>
    /// <param name="conn">Connection to a server.</param>
    /// <param name="errorCode">Error code.</param>
    public override void OnClientError(NetworkConnection conn, int errorCode) { }

    /// <summary>
    /// Called on clients when a servers tells the client it is no longer ready.
    /// <para>This is commonly used when switching scenes.</para>
    /// </summary>
    /// <param name="conn">Connection to the server.</param>
    public override void OnClientNotReady(NetworkConnection conn) { }

    /// <summary>
    /// Called from ClientChangeScene immediately before SceneManager.LoadSceneAsync is executed
    /// <para>This allows client to do work / cleanup / prep before the scene changes.</para>
    /// </summary>
    /// <param name="newSceneName">Name of the scene that's about to be loaded</param>
    /// <param name="sceneOperation">Scene operation that's about to happen</param>
    /// <param name="customHandling">true to indicate that scene loading will be handled through overrides</param>
    public override void OnClientChangeScene(string newSceneName, SceneOperation sceneOperation, bool customHandling) { }

    /// <summary>
    /// Called on clients when a scene has completed loaded, when the scene load was initiated by the server.
    /// <para>Scene changes can cause player objects to be destroyed. The default implementation of OnClientSceneChanged in the NetworkManager is to add a player object for the connection if no player object exists.</para>
    /// </summary>
    /// <param name="conn">The network connection that the scene change message arrived on.</param>
    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        base.OnClientSceneChanged(conn);
    }



    /// <summary>
    /// This is invoked when the client is started.
    /// </summary>
    public override void OnStartClient()
    {
        base.OnStartClient();
    }


    /// <summary>
    /// This is called when a client is stopped.
    /// </summary>
    public override void OnStopClient() { }
#endif
    #endregion

    #region Get Server Port & Network Address

    public override void Start()
    {
        base.Start();
        RequestMultiplayerServer();
    }

    public void RequestMultiplayerServer()
    {
        if (ClientMode == BuildMode.RemoteClient)
        {
            RequestMultiplayerServerRequest requestData = new RequestMultiplayerServerRequest();
            requestData.BuildId = ServerBuildID;
            requestData.SessionId = System.Guid.NewGuid().ToString();
            ///on the future, user will select region to play itself.
            requestData.PreferredRegions = ServerList.Instance.AvailableServer;
            PlayFabMultiplayerAPI.RequestMultiplayerServer(requestData, OnRequestMultiplayerServer, OnRequestMultiplayerServerError);
        }
        else
        {
            SetPortAndNetworkAddress();
        }
    }

    private void OnRequestMultiplayerServerError(PlayFabError obj)
    {
        Debug.Log(obj.ErrorDetails);
    }

    private void OnRequestMultiplayerServer(RequestMultiplayerServerResponse response)
    {
        if (response != null)
        {
            SetPortAndNetworkAddress(response);
        }
    }


    private void SetPortAndNetworkAddress(RequestMultiplayerServerResponse response = null)
    {
        if (ClientMode == BuildMode.RemoteClient)
        {
            networkAddress = response.IPV4Address;
            telepathy.port = (ushort)response.Ports[0].Num;
            kcp.Port = (ushort)response.Ports[0].Num;
        }
        else
        {
            networkAddress = ServerList.LOCALHOST;
            telepathy.port = ServerList.LOCALPORTS;
            kcp.Port = ServerList.LOCALPORTS;
        }
    }
    #endregion

}


public enum BuildMode
{
    LocalClient = 0,
    RemoteClient = 1,
}