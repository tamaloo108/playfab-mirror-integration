﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace STVR.CDR.ServerList
{
    public class ServerList
    {
        private static ServerList instance;
        public static ServerList Instance => instance;

        #region Region List
        //to be updated when ready.
        public const string EAST_US = "EastUs";
        public const string _NORTH_EU = "NorthEurope";
        #endregion

        #region Local Setup

        public const string LOCALHOST = "localhost";
        public const ushort LOCALPORTS = 56100;
        #endregion

        static ServerList()
        {

        }

        private ServerList()
        {

        }


        private List<string> availableServer = null;
        public List<string> AvailableServer
        {
            get
            {
                if (availableServer == null)
                {
                    SetAvaiableServer();
                }
                return availableServer;
            }
        }

        private void SetAvaiableServer()
        {
            availableServer = new List<string>
            {
                EAST_US
            };
        }
    }
}
