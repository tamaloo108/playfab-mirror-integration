using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Mirror;
using UnityEngine.InputSystem;

[RequireComponent(typeof(NavMeshAgent))]
public class UnitMovement : NetworkBehaviour
{
    [SerializeField] private NavMeshAgent _agent = null;
    private Camera mainCam;

    #region Server

    [Command]
    void CmdMove(Vector3 position)
    {
        if (!NavMesh.SamplePosition(position, out NavMeshHit hit, 1f, NavMesh.AllAreas))
        {
            return;
        }

        _agent.SetDestination(position);
    }

    #endregion

    #region Client

    public override void OnStartAuthority()
    {
        mainCam = Camera.main;
    }

    [ClientCallback]

    private void Update()
    {
        if (!hasAuthority) return;
        if (!Input.GetMouseButtonDown(0)) return;
        Ray r = mainCam.ScreenPointToRay(Input.mousePosition);
        if (!Physics.Raycast(r, out RaycastHit hit, Mathf.Infinity)) return;

        CmdMove(hit.point);
    }

    #endregion

}
